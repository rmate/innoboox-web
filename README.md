# innoboox web client

# install dependencies
npm install

# start application in development mode
npm run dev

# build for production with minification
npm run build
