import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import Vue from 'vue'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

import App from './App'
import router from './router'

Vue.use(Vuetify)
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    token: null,
    authenticated: false
  },
  mutations: {
    LOGIN_SUCCESS (state, response) {
      this.state.token = response.data
      this.state.authenticated = true
      localStorage.setItem('token', response.data)
      localStorage.setItem('authenticated', true)
    },
    LOGOUT () {
      localStorage.setItem('token', null)
      this.state.authenticated = false
      localStorage.setItem('authenticated', false)
    }
  }
})

export default store

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
