import Vue from 'vue'
import Vuex from 'vuex'
import Router from 'vue-router'
import Home from '@/components/Home'
import Header from '@/components/Header'
import Welcome from '@/components/Welcome'
import Borrow from '@/components/Borrow'
import Add from '@/components/Add'
import Login from '@/components/Login'

Vue.use(Vuex)
Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: Home,
        header: Header
      }
    },
    {
      path: '/welcome',
      name: 'welcome',
      components: {
        default: Welcome,
        header: Header
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/borrow',
      name: 'borrow',
      components: {
        default: Borrow,
        header: Header
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/add',
      name: 'add',
      components: {
        default: Add,
        header: Header
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      components: {
        default: Login,
        header: Header
      }
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!this.a.app.$store.state.authenticated) {
      next({
        path: '/',
        params: { nextUrl: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
