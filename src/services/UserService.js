import Api from '@/services/Api'

export default {
  login (params) {
    return Api().post('/login', params, {
      validateStatus (status) {
        return status === 200
      }
    })
  },
  getUsername () {
    return Api().get('/get-username', {
      validateStatus (status) {
        return status === 200
      }
    })
  }
}
