import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default () =>
  axios.create({
    baseURL: 'http://localhost:8080',
    withCredentials: false,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      get: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      post: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      put: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }
  })
