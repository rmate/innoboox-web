import Api from '@/services/Api'

export default {
  getBooks () {
    return Api().get('/get-books', {
      validateStatus (status) {
        return status === 200
      }
    })
  },

  getBooksByUser () {
    return Api().get('/get-books-by-user', {
      validateStatus (status) {
        return status === 200
      }
    })
  },

  borrowBook (params) {
    return Api().put('/borrow-book', params, {
      validateStatus (status) {
        return status === 200
      }
    })
  },

  returnBook (params) {
    return Api().put('/return-book', params, {
      validateStatus (status) {
        return status === 200
      }
    })
  },

  getCategories () {
    return Api().get('/get-categories', {
      validateStatus (status) {
        return status === 200
      }
    })
  },

  add (params) {
    return Api().post('/add-book', params, {
      validateStatus (status) {
        return status === 200
      }
    })
  }
}
